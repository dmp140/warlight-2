package main

import (
	"testing"
)

func TestBattleCalculation(t *testing.T) {
	if WillIWin(100, 0) == false {
		t.Error("100 will not lose against 0")
	}
	if WillIWin(100, 50) == false {
		t.Error("100 will at least kill 50 people.")
	}
	if WillIWin(100, 51) == true {
		t.Error("100 might not kill 51 people.")
	}
	if WillIWin(1, 1) == true {
		t.Error("1 can not beat 1.")
	}
}
