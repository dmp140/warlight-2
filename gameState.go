package main

import (
	//	"log"
	"strconv"
	"strings"
)

type SuperRegion struct {
	reward uint32
}

type Region struct {
	superRegionId    uint32
	adjacentRegions  map[uint32]bool
	owner            string
	numberOfSoldiers uint32
}

type State struct {
	timebank, timePerMove, maxRounds   string
	startingArmies, startingPickAmount uint32
	yourBot, opponentBot               string
	startingRegions                    []string
	opponentStartingRegions            []uint32
	myCurrentRegions                   []string
	superRegions                       map[uint32]SuperRegion
	regions                            map[uint32]Region
	wastelands                         []uint32
	numRegions                         uint32
	myNumRegions                       uint32
}

func NewState() *State {
	return &State{
		superRegions: make(map[uint32]SuperRegion), regions: make(map[uint32]Region),
	}
}
func ParseUpdateMap(state *State, line []string) {
	for key, value := range state.regions {
		value.owner = "unknown"
		value.numberOfSoldiers = 2
		state.regions[key] = value
	}
	//log.Println("updateMap", line)
	for x := 0; x < len(line)/3; x++ {
		regionId, _ := strconv.Atoi(line[x*3])
		soldiers, _ := strconv.Atoi(line[x*3+2])
		element := state.regions[uint32(regionId)]
		element.owner = line[x*3+1]
		element.numberOfSoldiers = uint32(soldiers)
		state.regions[uint32(regionId)] = element
	}
	state.myNumRegions = 0
	for _, value := range state.regions {
		if value.owner == state.yourBot {
			state.myNumRegions += 1
		}
	}
}

func ParseSettings(state *State, line []string) {
	var settingType string = line[0]
	var value string = line[1]
	switch settingType {
	case "your_bot":
		state.yourBot = value
	case "timebank":
		state.timebank = value
	case "time_per_move":
		state.timePerMove = value
	case "max_rounds":
		state.maxRounds = value
	case "opponent_bot":
		state.opponentBot = value
	case "starting_armies":
		val, _ := strconv.Atoi(value)
		state.startingArmies = uint32(val)
	case "starting_pick_amount":
		val, _ := strconv.Atoi(value)
		state.startingPickAmount = uint32(val)
	case "starting_regions":
		state.startingRegions = line[1:]
	default:
		panic("oh no" + settingType + value)
	}
}

func ParseSetup(state *State, line []string) {
	var settingType string = line[0]
	var value string = line[1]
	line = line[1:]
	switch settingType {
	case "super_regions":
		//log.Println(line)
		for i := 0; i < len(line)/2; i++ {
			superRegion, _ := strconv.Atoi(line[i*2])
			bonus, _ := strconv.Atoi(line[i*2+1])
			state.superRegions[uint32(superRegion)] = SuperRegion{uint32(bonus)}
		}
	case "regions":
		for i := 0; i < len(line)/2; i++ {
			regionId, _ := strconv.Atoi(line[i*2])
			superRegionId, _ := strconv.Atoi(line[i*2+1])
			state.regions[uint32(regionId)] = Region{
				superRegionId:    uint32(superRegionId),
				adjacentRegions:  make(map[uint32]bool),
				owner:            "unknown",
				numberOfSoldiers: 2,
			}
		}
		state.numRegions = uint32(len(state.regions))
	case "neighbors":
		for i := 0; i < len(line)/2; i++ {
			regionId, _ := strconv.Atoi(line[i*2])
			neighbors := strings.Split(line[i*2+1], ",")
			for _, neighbor := range neighbors {
				neigh, _ := strconv.Atoi(neighbor)
				state.regions[uint32(regionId)].adjacentRegions[uint32(neigh)] = true
				state.regions[uint32(neigh)].adjacentRegions[uint32(regionId)] = true
			}
		}

	case "wastelands":
		for _, val := range line {
			regionId, _ := strconv.Atoi(val)
			state.wastelands = append(state.wastelands, uint32(regionId))
			region := state.regions[uint32(regionId)]
			region.numberOfSoldiers = 6
		}
	case "opponent_starting_regions":
		for _, val := range line {
			regionId, _ := strconv.Atoi(val)
			state.opponentStartingRegions = append(state.opponentStartingRegions, uint32(regionId))
		}
	default:
		panic("oh no" + settingType + value)
	}
}
