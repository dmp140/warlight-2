package main

import (
	"testing"
)

func TestUpdateMapWillNoticeMissingRegions(t *testing.T) {
	state := NewState()
	state.yourBot = "player1"
	state.opponentBot = "player2"
	state.regions[1] = Region{1, make(map[uint32]bool), "player1", 2}
	state.regions[2] = Region{2, make(map[uint32]bool), "player1", 6}

	ParseUpdateMap(state, []string{"1", "player1", "1"})
	region := state.regions[1]
	if region.owner != "player1" || region.numberOfSoldiers != 1 {
		t.Error("Incorrect Region 1, player1 != ", region.owner, "1 != ", region.numberOfSoldiers)
	}
	region = state.regions[2]
	if region.owner != "player2" {
		t.Error("Incorrect Region 2, should now be controlled by player2 instead of: ", region.owner)
	}

}
