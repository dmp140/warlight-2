package main

import (
	"bufio"
	"fmt"
	//"log"
	//"math/rand"
	"os"
	"strings"
	//"time"
)

func main() {
	var state State = *NewState()
	bio := bufio.NewReader(os.Stdin)
	for {
		var line []string
		isPrefix := true
		var input string = ""
		for isPrefix {
			var byteArray []byte
			byteArray, isPrefix, _ = bio.ReadLine()
			input = input + string(byteArray[:])
		}
		line = strings.Split(input, " ")
		parseInput(&state, line)
	}

}

func parseInput(state *State, line []string) {
	var word string

	word, line = line[0], line[1:]
	//log.Println(word, line)
	switch word {
	case "close":
		os.Exit(0)
	case "pick_starting_region":
		var choice string = line[1]
		state.myCurrentRegions = append(state.myCurrentRegions, choice)
		fmt.Println(choice)
	case "settings":
		ParseSettings(state, line)
	case "setup_map":
		ParseSetup(state, line)
	case "go":
		word = line[0]
		if word == "place_armies" {
			pairs := PlaceUnits(state)
			for key, val := range pairs {
				fmt.Print(state.yourBot, " place_armies ", val.id, val.armies)
				if key < len(pairs)-1 {
					fmt.Print(", ")
				}
			}
			fmt.Println("")
		} else if word == "attack/transfer" {
			tuples := AttackTransfer(state)
			if len(tuples) == 0 {
				fmt.Println("No moves")
			} else {
				for key, val := range tuples {
					fmt.Print(state.yourBot, " attack/transfer ", val.fromId, val.toId, val.numUnits)
					if key < len(tuples)-1 {
						fmt.Print(", ")
					}
				}
				fmt.Println("")
			}
		}
	case "update_map":
		ParseUpdateMap(state, line)
	case "debug":
		fmt.Printf("%+v\r\n", state)
	}
}
