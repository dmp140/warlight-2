package main

import (
	"log"
)

var DEPTH uint32 = 7
var PLACE_DEPTH uint32 = 5
var PERCENT_SENT float32 = 0.75
var MAX_PLACE_ARMIES int = 5

type UnitPair struct {
	id, armies uint32
	priority   int32
}

func PlaceUnitsRecurse(state *State, id uint32, depth uint32, visited map[uint32]bool) int32 {
	region := state.regions[id]
	score := int32(0)
	if depth == 0 {
		return score
	}
	visited[id] = true
	for neighborId, _ := range region.adjacentRegions {
		_, ok := visited[neighborId]
		if !ok {
			prio := PlaceUnitsRecurse(state, neighborId, depth-1, visited)
			prio += scorePlaceRegion(state, id, neighborId, depth)
			if prio > score {
				score = prio
			}
		}
	}

	delete(visited, id)
	return score
}
func PlaceUnits(state *State) []UnitPair {
	if float32(state.numRegions)/float32(state.myNumRegions) > 50 {
		MAX_PLACE_ARMIES = 5
	} else if float32(state.numRegions)/float32(state.myNumRegions) > 40 {
		MAX_PLACE_ARMIES = 4
	} else if float32(state.numRegions)/float32(state.myNumRegions) > 30 {
		MAX_PLACE_ARMIES = 3
	} else if float32(state.numRegions)/float32(state.myNumRegions) > 20 {
		MAX_PLACE_ARMIES = 2
	} else {
		MAX_PLACE_ARMIES = 1
	}

	var retVal []UnitPair
	for regionId, region := range state.regions {
		if region.owner == state.yourBot {
			visited := make(map[uint32]bool)
			priority := PlaceUnitsRecurse(state, regionId, 1, visited)
			if len(retVal) < MAX_PLACE_ARMIES {
				retVal = append(retVal, UnitPair{regionId, 0, priority})
			} else {
				var lowest int32 = int32(^uint32(0) >> 1) //MaxInt
				var lowestIndex = 0
				for index, val := range retVal {
					if val.priority < lowest {
						lowest = val.priority
						lowestIndex = index
					}
				}
				if priority > lowest {
					retVal[lowestIndex] = UnitPair{regionId, 0, priority}
				}
			}
		}
	}

	for i := uint32(0); i < state.startingArmies; i++ {
		something := retVal[i%uint32(len(retVal))]
		something.armies += 1
		retVal[i%uint32(len(retVal))] = something
	}

	for _, val := range retVal {
		element := state.regions[val.id]
		element.numberOfSoldiers += val.armies
		state.regions[val.id] = element
	}

	return retVal
}

type AttackTransferTuple struct {
	fromId, toId, numUnits uint32
}

func AttackTransferRecurse(state *State, id uint32, numberOfUnits uint32, depth uint32, visited map[uint32]bool) (int32, AttackTransferTuple) {
	region := state.regions[id]
	score := int32(0)
	retPrio, retTuple := int32(0), AttackTransferTuple{id, id, numberOfUnits}
	if depth == 0 {
		return retPrio, retTuple
	}
	visited[id] = true
	for neighborId, _ := range region.adjacentRegions {
		_, ok := visited[neighborId]
		if !ok {
			prio, _ := AttackTransferRecurse(state, neighborId, uint32(float32(numberOfUnits)*PERCENT_SENT), depth-1, visited)
			prio += scoreRegion(state, id, neighborId, depth)
			if prio > score {
				retPrio, retTuple = prio, AttackTransferTuple{id, neighborId, uint32(float32(numberOfUnits) * PERCENT_SENT)}
				score = prio
			}
		}
	}
	delete(visited, id)
	return retPrio, retTuple
}

func AttackTransfer(state *State) []AttackTransferTuple {
	if float32(state.numRegions)/float32(state.myNumRegions) > 90 {
		PERCENT_SENT = 1
	} else if float32(state.numRegions)/float32(state.myNumRegions) > 80 {
		PERCENT_SENT = .90
	} else {
		PERCENT_SENT = .80
	}
	var retVal []AttackTransferTuple
	for regionId, region := range state.regions {
		if region.owner == state.yourBot && region.numberOfSoldiers > 3 {
			visited := make(map[uint32]bool)
			_, tuple := AttackTransferRecurse(state, regionId, uint32(float32(region.numberOfSoldiers)*PERCENT_SENT), DEPTH, visited)
			log.Println(tuple)
			if tuple.fromId != tuple.toId && tuple.numUnits != 0 {
				retVal = append(retVal, tuple)
			}
		}
	}
	return retVal
}

func shouldINotAttackThis(myRegion uint32, otherRegion uint32, state *State) bool {
	myReg := state.regions[myRegion]
	otherReg := state.regions[otherRegion]
	if otherReg.owner != state.yourBot && (!WillIWin(uint32(float32(myReg.numberOfSoldiers)*PERCENT_SENT), otherReg.numberOfSoldiers)) {
		return true
	}
	return false
}
func shouldIAttackThis(myRegion uint32, otherRegion uint32, state *State) bool {
	myReg := state.regions[myRegion]
	otherReg := state.regions[otherRegion]
	if otherReg.owner != state.yourBot && (WillIWin(uint32(float32(myReg.numberOfSoldiers)*PERCENT_SENT), otherReg.numberOfSoldiers)) {
		return true
	}
	return false
}

func round(iHateYouTheMostGo float32) uint32 {
	return uint32(iHateYouTheMostGo + 0.5)
}

func WillIWin(myUnits uint32, theirUnits uint32) bool {
	defendersLost := (float32(myUnits)*0.6)*.84 + 0*0.16
	attackersLost := (float32(theirUnits)*0.7)*.84 + float32(theirUnits)*0.16
	log.Println(defendersLost, round(defendersLost), attackersLost, round(attackersLost))
	return (round(defendersLost) >= theirUnits && round(attackersLost) < myUnits)

}

func scoreRegion(state *State, myRegionId uint32, otherRegionId uint32, depth uint32) int32 {
	var prio int32 = 0
	var ratio int32 = int32(depth)
	otherReg := state.regions[otherRegionId]
	myReg := state.regions[myRegionId]
	if state.opponentBot == otherReg.owner && shouldIAttackThis(myRegionId, otherRegionId, state) {
		prio += (100 * ratio)
	} else if otherReg.owner != state.opponentBot && otherReg.owner != state.yourBot && shouldIAttackThis(myRegionId, otherRegionId, state) {
		prio += (50 * ratio)
	} else if otherReg.owner != state.yourBot && shouldINotAttackThis(myRegionId, otherRegionId, state) {
		prio -= (1000 * ratio)
	} else if otherReg.owner == state.yourBot && otherReg.numberOfSoldiers > myReg.numberOfSoldiers {
		prio += (10 * ratio)
	} else {
		prio += (1 * ratio)
	}
	return prio
}

func scorePlaceRegion(state *State, myRegionId uint32, otherRegionId uint32, depth uint32) int32 {
	var prio int32 = 0
	var ratio int32 = int32(depth)
	otherReg := state.regions[otherRegionId]
	if state.opponentBot == otherReg.owner && shouldIAttackThis(myRegionId, otherRegionId, state) {
		prio += (4 * ratio)
	} else if state.opponentBot == otherReg.owner && otherReg.numberOfSoldiers == 1 {
		prio += (6 * ratio)
	} else if state.opponentBot == otherReg.owner {
		prio += (5 * ratio)
	} else if otherReg.owner != state.opponentBot && otherReg.owner != state.yourBot {
		prio += (3 * ratio)
	} else if otherReg.owner == state.yourBot {
		prio += (2 * ratio)
	}
	return prio
}
